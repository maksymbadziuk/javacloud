APIS:
-----

Rabbit MQ (port 15672)		            -> http://localhost:15672/#/connections
Zipkin    (port 9411)                   -> http://localhost:9411/zipkin/


Spring Cloud Config Server  (port 8888)     -> http://localhost:8888/limits-service/container, http://localhost:8888/currency-conversion-service/container

Eureka                      (port 8761)     -> http://localhost:8761/

Spring Cloud Bus Refresh    (port 15672)    -> http://localhost:8080/actuator/bus-refresh   (POST via curl or PostMan)




Limits Service              (port 15672)    -> http://localhost:8080/limits

Currency Exchange Service   (port 8000)     -> http://localhost:8000/currency-exchange/from/EUR/to/IMR

            (via 8765 Zuul Api GateWay)     -> http://localhost:8765/currency-exchange-service/currency-exchange/from/EUR/to/IMR


Currency Converter Service  (port 8100)    -> http://localhost:8100/currency-conversion/v1/from/EUR/to/IMR/quantity/10

            (via 8765 Zuul Api GateWay)     -> http://localhost:8765/currency-conversion-service/currency-conversion/v1/from/EUR/to/IMR/quantity/10

                                            ->http://localhost:8100/currency-conversion/v1/products  (for MongoDB)