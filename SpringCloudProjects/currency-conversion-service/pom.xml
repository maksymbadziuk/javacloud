<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.mycloud</groupId>
	<artifactId>currency-conversion-service</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>currency-conversion-service</name>
	<description>Conversion service</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.0.6.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<spring-cloud.version>Finchley.SR2</spring-cloud.version>

		<!-- Docker account name-->
		<docker.image.prefix>maksymbadziuk</docker.image.prefix>
		<!-- Name of the project -->
		<docker.image.name>${project.name}</docker.image.name>

		<!--Create and remove custom networks for Docker -->
		<docker.autoCreateCustomNetworks>true</docker.autoCreateCustomNetworks>
		<my.docker.network.name>my-net</my.docker.network.name>

		<!-- Container naming strategy -->
		<docker.namingStrategy>alias</docker.namingStrategy>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-mongodb</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-openfeign</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-sleuth</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-sleuth-zipkin</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-bus-amqp</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.retry</groupId>
			<artifactId>spring-retry</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-aop</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>

			<plugin>
				<groupId>io.fabric8</groupId>
				<artifactId>docker-maven-plugin</artifactId>
				<version>0.20.0</version>

				<configuration>

					<!--<dockerHost>http://127.0.0.1:2375</dockerHost>-->
					<dockerHost>unix:///var/run/docker.sock</dockerHost>
					<verbose>true</verbose>

					<images>

						<!-- Rabbit container -->
						<image>
							<name>rabbitmq:3-management</name>
							<alias>rabbitmq</alias>
							<run>
								<namingStrategy>alias</namingStrategy>
								<ports>
									<port>5671</port>
									<port>5672:5672</port>
									<port>4369</port>
									<port>25672</port>
									<port>15672:15672</port>
								</ports>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
							</run>
						</image>

						<!-- Zipkin distributed tracing server -->
						<image>
							<name>openzipkin/zipkin</name>
							<alias>zipkin</alias>
							<run>
								<namingStrategy>alias</namingStrategy>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
								<ports>
									<port>9411:9411</port>
								</ports>
								<env>
									<SPRING_RABBITMQ_HOST>rabbitmq</SPRING_RABBITMQ_HOST>
									<RABBIT_URI>amqp://rabbitmq</RABBIT_URI>
								</env>
								<dependsOn>
									<container>rabbitmq</container>
								</dependsOn>
							</run>
						</image>

						<!-- MySQL container -->
						<image>
							<name>mysql</name>
							<alias>mysql</alias>
							<run>
								<namingStrategy>alias</namingStrategy>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
								<ports>
									<port>3306:3306</port>
								</ports>
								<env>
									<MYSQL_ALLOW_EMPTY_PASSWORD>yes</MYSQL_ALLOW_EMPTY_PASSWORD>
								</env>
								<volumes>
									<bind>
										<volume>/Users/maksymbadziuk/myProj/DockerData/mysql:/var/lib/mysql</volume>
									</bind>
								</volumes>
							</run>
						</image>

						<!-- MONGO Db container -->
						<image>
							<name>mongo</name>
							<alias>mongo</alias>
							<run>
								<namingStrategy>alias</namingStrategy>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
								<ports>
									<port>27017:27017</port>
								</ports>
								<volumes>
									<bind>
										<volume>/Users/maksymbadziuk/myProj/DockerData/mongo:/etc/mongo</volume>
									</bind>
								</volumes>
							</run>
						</image>

						<!-- Spring cloud config service container-->
						<image>
							<name>${docker.image.prefix}/spring-cloud-config-server</name>
							<alias>spring-cloud-config-server</alias>

							<run>
								<namingStrategy>alias</namingStrategy>
								<ports>
									<port>8888:8888</port>
								</ports>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
								<env>
									<SPRING_RABBITMQ_HOST>rabbitmq</SPRING_RABBITMQ_HOST>
								</env>
								<dependsOn>
									<container>zipkin</container>
								</dependsOn>
							</run>
						</image>

						<!-- Netflix eureka service container-->
						<image>
							<name>${docker.image.prefix}/netflix-eureka-naming-server</name>
							<alias>netflix-eureka-naming-server</alias>
							<run>
								<namingStrategy>alias</namingStrategy>
								<ports>
									<port>8761:8761</port>
								</ports>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
								<dependsOn>
									<container>spring-cloud-config-server</container>
								</dependsOn>
							</run>
						</image>

						<!-- Zuul Api Gateway server container-->
						<image>
							<name>${docker.image.prefix}/netflix-zuul-api-gateway-server</name>
							<alias>netflix-zuul-api-gateway-server</alias>
							<run>
								<namingStrategy>alias</namingStrategy>
								<ports>
									<port>8765:8765</port>
								</ports>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
								<env>
									<SPRING_RABBITMQ_HOST>rabbitmq</SPRING_RABBITMQ_HOST>
								</env>
								<dependsOn>
									<container>netflix-eureka-naming-server</container>
								</dependsOn>
							</run>
						</image>

						<!-- Limit service container-->
						<image>
							<name>${docker.image.prefix}/limits-service</name>
							<alias>limits-service</alias>
							<run>
								<namingStrategy>alias</namingStrategy>
								<ports>
									<port>8080:8080</port>
								</ports>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
								<env>
									<SPRING_RABBITMQ_HOST>rabbitmq</SPRING_RABBITMQ_HOST>
								</env>
								<dependsOn>
									<container>netflix-zuul-api-gateway-server</container>
								</dependsOn>
							</run>
						</image>

						<!-- Currency Exchange Service container-->
						<image>
							<name>${docker.image.prefix}/currency-exchange-service</name>
							<alias>currency-exchange-service</alias>
							<run>
								<namingStrategy>alias</namingStrategy>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
								<ports>
									<port>8000:8000</port>
								</ports>
								<env>
									<SPRING_RABBITMQ_HOST>rabbitmq</SPRING_RABBITMQ_HOST>
								</env>
								<dependsOn>
									<container>limits-service</container>
								</dependsOn>
							</run>
						</image>

						<!-- Currency Conversion Service container-->
						<image>
							<name>${docker.image.prefix}/${docker.image.name}</name>
							<alias>${docker.image.name}</alias>
							<build>
								<dockerFileDir>${project.basedir}/src/main/docker/</dockerFileDir>

								<!-- copies artifact to docker build dir in target -->
								<assembly>
									<descriptorRef>artifact</descriptorRef>
								</assembly>
								<tags>
									<tag>latest</tag>
									<tag>${project.version}</tag>
								</tags>
							</build>
							<run>
								<namingStrategy>alias</namingStrategy>
								<network>
									<mode>custom</mode>
									<name>${my.docker.network.name}</name>
								</network>
								<ports>
									<port>8100:8100</port>
								</ports>
								<env>
									<SPRING_RABBITMQ_HOST>rabbitmq</SPRING_RABBITMQ_HOST>
									<SPRING_DATA_MONGODB_HOST>mongo</SPRING_DATA_MONGODB_HOST>
								</env>
								<dependsOn>
									<container>currency-exchange-service</container>
								</dependsOn>
							</run>
						</image>

					</images>
				</configuration>

				<!-- Integration test runner -->
				<executions>
					<execution>
						<id>start</id>
						<phase>pre-integration-test</phase>
						<goals>
							<!-- "build" should be used to create the images with the
                                 artifact -->
							<goal>build</goal>
							<goal>start</goal>
						</goals>
					</execution>
					<execution>
						<id>stop</id>
						<phase>post-integration-test</phase>
						<goals>
							<goal>stop</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<version>2.19.1</version>
				<executions>
					<execution>
						<id>integration-test</id>
						<goals>
							<goal>integration-test</goal>
							<goal>verify</goal>
						</goals>
					</execution>
				</executions>

			</plugin>
		</plugins>
	</build>


</project>
