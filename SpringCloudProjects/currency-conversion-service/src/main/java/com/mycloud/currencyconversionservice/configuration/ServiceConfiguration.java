package com.mycloud.currencyconversionservice.configuration;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = {"com.mycloud.currencyconversionservice", "com.mycloud.currencyconversionservice.proxy"})
public class ServiceConfiguration {
}
