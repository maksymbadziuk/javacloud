package com.mycloud.currencyconversionservice.controller;

import com.mycloud.currencyconversionservice.model.CurrencyConversion;
import com.mycloud.currencyconversionservice.model.Product;
import com.mycloud.currencyconversionservice.proxy.CurrencyExchangeServiceProxy;
import com.mycloud.currencyconversionservice.services.ProductService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class CurrencyConversionController {

    private Logger LOG = LoggerFactory.getLogger(CurrencyConversionController.class);

    @Autowired
    Environment environment;

    @Autowired
    CurrencyExchangeServiceProxy exchangeServiceProxy;

    @Autowired
    ProductService productService;

    @PostConstruct
    public void initIt() {
        if (productService.listAll().isEmpty()) {
            final Product product1 = new Product(new ObjectId(), "CurrencyExchange", new BigDecimal(0.05));
            final Product product2 = new Product(new ObjectId(), "OldCurrencyExchange", new BigDecimal(0.2));
            final Product product3 = new Product(new ObjectId(), "CurrencyValidation", new BigDecimal(0.01));
            productService.saveOrUpdate(product1);
            productService.saveOrUpdate(product2);
            productService.saveOrUpdate(product3);
        }
    }

    @GetMapping("/currency-conversion/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversion convertCurrency(
            @PathVariable final String from,
            @PathVariable final String to,
            @PathVariable Integer quantity) {

        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("from", from);
        uriVariables.put("to", to);

        final ResponseEntity<CurrencyConversion> responseEntity = new RestTemplate().getForEntity(
                "http://localhost:8000/currency-exchange/from/{from}/to/{to}",
                CurrencyConversion.class,
                uriVariables);

        final CurrencyConversion conversion = responseEntity.getBody();
        conversion.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
        conversion.setQuantity(new BigDecimal(quantity));
        conversion.setTotalCalculatedAmount(conversion.getConversionMultiple().multiply(conversion.getQuantity()));



        return conversion;
    }

    @GetMapping("/currency-conversion/v1/from/{from}/to/{to}/quantity/{quantity}")
    @HystrixCommand(fallbackMethod = "convertCurrencyV1Fallback")
    public CurrencyConversion convertCurrencyV1(
            @PathVariable final String from,
            @PathVariable final String to,
            @PathVariable Integer quantity) {

        final int port = Integer.parseInt(environment.getProperty("local.server.port"));

        final CurrencyConversion response = exchangeServiceProxy.getRate(from, to);
        final CurrencyConversion currencyConversion = new CurrencyConversion(
                from,
                to,
                response.getConversionMultiple(),
                new BigDecimal(quantity),
                response.getConversionMultiple().multiply(new BigDecimal(quantity)),
                response.getPort());


        LOG.info("Conversion response -> {}", currencyConversion);

        return currencyConversion;
    }

    @GetMapping("/currency-conversion/v1/products")
    @HystrixCommand(fallbackMethod = "productsFallback")
    public List<Product> getProducts() {
        return productService.listAll();
    }

    @PostMapping("/currency-conversion/v1/products")
    public List<Product> saveProduct(Product product) {
        product.setId(new ObjectId());
        product = productService.saveOrUpdate(product);
        return productService.listAll();
    }



    // FAULT TOLERANT FALLBACKS (Netflix hystrix)
    public List<Product> productsFallback() {
        final Product product = new Product();
        product.setId(new ObjectId());
        product.setCommission(new BigDecimal(0.0));
        product.setDescription("Default product");
        return Arrays.asList(product);
    }

    public CurrencyConversion convertCurrencyV1Fallback(String from, final String to, Integer quantity) {
        return new CurrencyConversion(
                from,
                to,
                new BigDecimal(0),
                new BigDecimal(quantity),
                new BigDecimal(0),
                Integer.parseInt(environment.getProperty("local.server.port")));
    }
}
