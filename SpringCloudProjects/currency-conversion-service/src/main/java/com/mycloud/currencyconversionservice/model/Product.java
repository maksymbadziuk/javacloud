package com.mycloud.currencyconversionservice.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document
public class Product {
    @Id
    private ObjectId _id;
    private String description;
    private BigDecimal commission;

    public Product() {
    }

    public Product(ObjectId _id, String description, BigDecimal commission) {
        this._id = _id;
        this.description = description;
        this.commission = commission;
    }

    public ObjectId getId() {
        return _id;
    }

    public void setId(ObjectId id) {
        this._id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }
}
