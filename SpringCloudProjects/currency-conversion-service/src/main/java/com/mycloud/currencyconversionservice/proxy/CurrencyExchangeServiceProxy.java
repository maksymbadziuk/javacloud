package com.mycloud.currencyconversionservice.proxy;

import com.mycloud.currencyconversionservice.model.CurrencyConversion;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "currency-exchange-service")  // Rest Http client
@FeignClient(name = "netflix-zuul-api-gateway-server")  // Rest Http client through API Gateway
@RibbonClient(name = "currency-exchange-service") // Load balancing on client for Rest calls
public interface CurrencyExchangeServiceProxy {

//    @GetMapping("/currency-exchange/from/{fromCurrency}/to/{toCurrency}")
    @GetMapping("/currency-exchange-service/currency-exchange/from/{fromCurrency}/to/{toCurrency}")
    CurrencyConversion getRate(@PathVariable("fromCurrency") final String fromCurrency, @PathVariable("toCurrency") final String toCurrency);
}
