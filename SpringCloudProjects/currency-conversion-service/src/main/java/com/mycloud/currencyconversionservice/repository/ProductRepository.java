package com.mycloud.currencyconversionservice.repository;

import com.mycloud.currencyconversionservice.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, String> {
}
