version: '3.1'

networks:
  my-net:
    driver: overlay

services:

  spring-cloud-config-server:
    image: maksymbadziuk/spring-cloud-config-server
    restart: always
    ports:
      - 8888:8888
    environment:
      SPRING_RABBITMQ_HOST: 'rabbitmq'
    networks:
      - my-net
    deploy:
      replicas: 1

  rabbitmq:
    image: rabbitmq:3-management
    restart: always
    ports:
      - 5671
      - 5672:5672
      - 4369
      - 25672
      - 15672:15672
    networks:
      - my-net
    deploy:
      replicas: 1

  zipkin:
    image: openzipkin/zipkin
    restart: always
    depends_on:
      - rabbitmq
    ports:
      - 9411:9411
    environment:
      SPRING_RABBITMQ_HOST: 'rabbitmq'
      RABBIT_URI: 'amqp://rabbitmq'
    networks:
      - my-net
    deploy:
      replicas: 1

  mysql:
    image: mysql
    restart: always
    ports:
      - 3306:3306
    environment:
      MYSQL_ALLOW_EMPTY_PASSWORD: 'yes'
    networks:
      - my-net
    deploy:
      replicas: 1

  mongo:
    image: mongo
    restart: always
    ports:
      - 27017:27017
    networks:
      - my-net
    deploy:
      replicas: 1

  netflix-eureka-naming-server:
    image: maksymbadziuk/netflix-eureka-naming-server
    restart: always
    depends_on:
      - spring-cloud-config-server
      - zipkin
    ports:
      - 8761:8761
    environment:
      SPRING_RABBITMQ_HOST: 'rabbitmq'
    networks:
      - my-net
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8761/actuator/health"]
      interval: 20s
      timeout: 10s
      retries: 3
    deploy:
      replicas: 1

  netflix-zuul-api-gateway-server:
    image: maksymbadziuk/netflix-zuul-api-gateway-server
    restart: always
    depends_on:
      - netflix-eureka-naming-server
    ports:
      - 8765:8765
    environment:
      SPRING_RABBITMQ_HOST: 'rabbitmq'
    networks:
      - my-net
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8765/actuator/health"]
      interval: 20s
      timeout: 10s
      retries: 3
    deploy:
      replicas: 1

  limits-service:
    image: maksymbadziuk/limits-service
    restart: always
    depends_on:
      - netflix-zuul-api-gateway-server
    ports:
      - 8080:8080
    environment:
      SPRING_RABBITMQ_HOST: 'rabbitmq'
    networks:
      - my-net
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8080/actuator/health"]
      interval: 60s
      timeout: 20s
      retries: 3
    deploy:
      replicas: 1

  currency-exchange-service:
    image: maksymbadziuk/currency-exchange-service
    restart: always
    depends_on:
      - limits-service
    ports:
      - 8000:8000
      - 8001:8001
    environment:
      SPRING_RABBITMQ_HOST: 'rabbitmq'
      JAVA_TOOL_OPTIONS: '-agentlib:jdwp=transport=dt_socket,address=8001,server=y,suspend=n'
    networks:
      - my-net
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8000/actuator/health"]
      interval: 65s
      timeout: 20s
      retries: 3
    deploy:
      replicas: 1

  currency-conversion-service:
    image: maksymbadziuk/currency-conversion-service
    restart: always
    depends_on:
      - currency-exchange-service
    ports:
      - 8100:8100
      - 8101:8101
    environment:
      SPRING_RABBITMQ_HOST: 'rabbitmq'
      SPRING_DATA_MONGODB_HOST: 'mongo'
      JAVA_TOOL_OPTIONS: '-agentlib:jdwp=transport=dt_socket,address=8101,server=y,suspend=n'
    networks:
      - my-net
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8100/actuator/health"]
      interval: 80s
      timeout: 20s
      retries: 3
    deploy:
      replicas: 1