
# Create an OVERLAY network for constainer-to-container communication in cluster
docker network create --driver overlay my-net

# Run CLOUD CONFIG WS as Docker Service on cluster
docker service create \
--name spring-cloud-config-server \
-p 8888:8888 \
-e SPRING_RABBITMQ_HOST=rabbitmq \
--network my-net \
maksymbadziuk/spring-cloud-config-server


# Run RABBIT MQ as Docker Service on cluster
docker service create \
--name rabbitmq \
-p 15672:15672 \
-p 5672:5672 \
--network my-net \
rabbitmq:3-management

# Run ZIPKIN distributed tracing as Docker Service on cluster
docker service create \
--name zipkin \
-p 9411:9411 \
-e SPRING_RABBITMQ_HOST=rabbitmq \
-e RABBIT_URI=amqp://rabbitmq \
--network my-net \
openzipkin/zipkin

# Run LIMITS SERVICE WS as Docker Service on cluster
docker service create \
--name limits-service \
-p 8080:8080 \
-e SPRING_RABBITMQ_HOST=rabbitmq \
--network my-net \
maksymbadziuk/limits-service