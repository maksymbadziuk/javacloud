package com.mycloud.currencyconversionservice;

import org.junit.Test;

public class SomeIntegrationTestIT {

    @Test
    public void someFakeTestOne() throws Exception {
        System.out.println("Test 1 .");

        Thread.sleep(100);

        System.out.println("Test 1 . .");

        Thread.sleep(100);

        System.out.println("Test 1 . .");

        Thread.sleep(100);

        System.out.println("Test 1 . . .");

        Thread.sleep(100);

        System.out.println("Test 1 . . . .");

        Thread.sleep(100);

        System.out.println("Test 1 . . . . .");
    }

    @Test
    public void someFakeTestTwo() throws Exception {
        System.out.println("Test 2 .");

        Thread.sleep(100);

        System.out.println("Test 2 . .");

        Thread.sleep(100);

        System.out.println("Test 2 . .");

        Thread.sleep(100);

        System.out.println("Test 2 . . .");

        Thread.sleep(100);

        System.out.println("Test 2 . . . .");

        Thread.sleep(100);

        System.out.println("Test 2 . . . . .");
    }

    @Test
    public void someFakeTestThree() throws Exception {
        System.out.println("Test 3 .");

        Thread.sleep(100);

        System.out.println("Test 3 . .");

        Thread.sleep(100);

        System.out.println("Test 3 . .");

        Thread.sleep(100);

        System.out.println("Test 3 . . .");

        Thread.sleep(100);

        System.out.println("Test 3 . . . .");

        Thread.sleep(100);

        System.out.println("Test 3 . . . . .");
    }
}
