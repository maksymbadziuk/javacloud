package com.mycloud.currencyexchangeservice.controller;

import com.mycloud.currencyexchangeservice.exception.NoSuchRateException;
import com.mycloud.currencyexchangeservice.model.ExchangeRate;
import com.mycloud.currencyexchangeservice.model.ExchangeRateId;
import com.mycloud.currencyexchangeservice.model.LimitConfiguration;
import com.mycloud.currencyexchangeservice.proxy.LimitsServiceRestProxy;
import com.mycloud.currencyexchangeservice.repository.ExchangeServiceRepository;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@RestController
public class CurrencyExchangeController {

    public static final Logger LOG = LoggerFactory.getLogger(CurrencyExchangeController.class);


    @Autowired
    private Environment environment;

    @Autowired
    private ExchangeServiceRepository exchangeServiceRepository;

    @Autowired
    private LimitsServiceRestProxy limitsServiceRestProxy;

    @PostConstruct
    public void initDbValues() {
        if (getRates().isEmpty()) {
            ExchangeRate rate1 = new ExchangeRate("EUR", "USD", new BigDecimal(1.14));
            ExchangeRate rate2 = new ExchangeRate("EUR", "IMR", new BigDecimal(65));
            ExchangeRate rate3 = new ExchangeRate("USD", "EUR", new BigDecimal(0.86));
            exchangeServiceRepository.saveAll(Arrays.asList(rate1, rate2, rate3));
            exchangeServiceRepository.flush();
        }
    }

    @GetMapping("/currency-exchange/from/{fromCurrency}/to/{toCurrency}")
    @HystrixCommand(fallbackMethod = "getRateFallback")
    public ExchangeRate getRate(@PathVariable("fromCurrency") final String fromCurrency, @PathVariable("toCurrency") final String toCurrency) {
        final ExchangeRate rate = exchangeServiceRepository.findByExchangeRateId(new ExchangeRateId(fromCurrency, toCurrency));

        if (rate == null) {
            throw new NoSuchRateException("NoSuchRate: FROM  " + fromCurrency + " TO " + toCurrency);
        }

        final LimitConfiguration limits = limitsServiceRestProxy.getLimits();
        LOG.info("!!!!  " + limits);

        // call limits service
        // get rates from DB
        rate.setPort(Integer.parseInt(environment.getProperty("local.server.port")));

        return rate;
    }


    @GetMapping("/currency-exchange")
    public List<ExchangeRate> getRates() {
        return exchangeServiceRepository.findAll();
    }

    public ExchangeRate getRateFallback(final String fromCurrency, final String toCurrency) {
        return getRates().get(0);
    }


}
