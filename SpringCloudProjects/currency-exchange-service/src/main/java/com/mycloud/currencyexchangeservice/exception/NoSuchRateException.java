package com.mycloud.currencyexchangeservice.exception;

public class NoSuchRateException extends RuntimeException {
    public NoSuchRateException(String noSuchRate) {
        super(noSuchRate);
    }
}
