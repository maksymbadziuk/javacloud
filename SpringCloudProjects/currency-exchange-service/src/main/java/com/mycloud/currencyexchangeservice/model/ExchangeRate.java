package com.mycloud.currencyexchangeservice.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name="EXCHANGE_RATE")
public class ExchangeRate implements Serializable {

    @JsonIgnore
    @EmbeddedId
    private ExchangeRateId exchangeRateId;

    @Column(name="exchange_rate")
    private BigDecimal conversionMultiple;

    @Transient
    private int port;

    public ExchangeRate() {
    }

    public ExchangeRateId getExchangeRateId() {
        return exchangeRateId;
    }

    public void setExchangeRateId(ExchangeRateId exchangeRateId) {
        this.exchangeRateId = exchangeRateId;
    }

    public ExchangeRate(String fromCurrencyId, String toCurrencyId, BigDecimal exchangeRate) {
        this.exchangeRateId = new ExchangeRateId(fromCurrencyId, toCurrencyId);
        this.conversionMultiple = exchangeRate;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getFrom() {
        return exchangeRateId.getFromCurrencyId();
    }

    public String getTo() {
        return exchangeRateId.getToCurrencyId();
    }

    public BigDecimal getConversionMultiple() {
        return conversionMultiple;
    }

    public void setConversionMultiple(BigDecimal conversionMultiple) {
        this.conversionMultiple = conversionMultiple;
    }
}
