package com.mycloud.currencyexchangeservice.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ExchangeRateId implements Serializable {

    @Column(name = "from_currency_id", length = 20)
    private String fromCurrencyId;

    @Column(name="to_currency_id", length = 20)
    private String toCurrencyId;

    public ExchangeRateId() {
    }

    public ExchangeRateId(String fromCurrencyId, String toCurrencyId) {
        this.fromCurrencyId = fromCurrencyId;
        this.toCurrencyId = toCurrencyId;
    }

    public String getFromCurrencyId() {
        return fromCurrencyId;
    }

    public void setFromCurrencyId(String fromCurrencyId) {
        this.fromCurrencyId = fromCurrencyId;
    }

    public String getToCurrencyId() {
        return toCurrencyId;
    }

    public void setToCurrencyId(String toCurrencyId) {
        this.toCurrencyId = toCurrencyId;
    }
}
