package com.mycloud.currencyexchangeservice.model;

public class LimitConfiguration {

    private int min;
    private int max;

    public LimitConfiguration() {
    }

    public LimitConfiguration(int max, int min) {
        this.max = max;
        this.min = min;

    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    @Override
    public String toString() {
        return "LimitConfiguration{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }
}
