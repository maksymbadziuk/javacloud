package com.mycloud.currencyexchangeservice.proxy;

import com.mycloud.currencyexchangeservice.model.LimitConfiguration;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

//@FeignClient(name = "limits-service")
@FeignClient(name = "netflix-zuul-api-gateway-server")
@RibbonClient(name = "limits-service")
public interface LimitsServiceRestProxy {

//    @GetMapping("/limits")
    @GetMapping("/limits-service/limits")
    LimitConfiguration getLimits();
}
