package com.mycloud.currencyexchangeservice.repository;

import com.mycloud.currencyexchangeservice.model.ExchangeRate;
import com.mycloud.currencyexchangeservice.model.ExchangeRateId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExchangeServiceRepository extends JpaRepository<ExchangeRate, ExchangeRateId> {
    public ExchangeRate findByExchangeRateId(final ExchangeRateId fromToCurrency);
//    public List<ExchangeRate> fin findBy AllByExchangeRateId(final ExchangeRateId fromToCurrency);
}
