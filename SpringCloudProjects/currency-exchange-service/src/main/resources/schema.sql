create table exchange_rate (from_currency_id varchar(255) not null, to_currency_id varchar(255) not null, exchange_rate decimal(19,2), primary key (from_currency_id, to_currency_id));
