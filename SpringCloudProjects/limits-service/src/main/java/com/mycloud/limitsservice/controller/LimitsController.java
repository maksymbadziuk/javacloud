package com.mycloud.limitsservice.controller;

import com.mycloud.limitsservice.configuration.Configuration;
import com.mycloud.limitsservice.model.LimitConfiguration;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitsController {

    private Logger LOG = LoggerFactory.getLogger(LimitsController.class);

//    @Value("${limits-service.max}")
//    private Integer max;
//
//    @Value("${limits-service.min}")
//    private Integer min;

    @Autowired
    private Configuration config;

    @GetMapping("/limits")
    public LimitConfiguration getLimits() {

        final LimitConfiguration limitConfiguration = new LimitConfiguration(config.getMax(), config.getMin());

        LOG.info("!!!!  " + limitConfiguration);

        return limitConfiguration;
    }

    @GetMapping("/faulty-limits")
    @HystrixCommand(fallbackMethod = "fallbackRetrieveLimits")
    public LimitConfiguration getCascadeFailure() {
        throw new RuntimeException("Limits Service is not available!");
    }

    public LimitConfiguration fallbackRetrieveLimits() {
        return new LimitConfiguration(1000, 1);
    }
}
